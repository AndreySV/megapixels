#ifdef GL_ES
precision highp float;
#endif

uniform sampler2D texture;
uniform mat3 color_matrix;
uniform float inv_gamma;
uniform float blacklevel;

varying vec2 texture_coord;

void
main()
{
    // Sample format: Y,U,Y,V
    vec4 raw = texture2D(texture, texture_coord);
    #if defined(FMT_YUYV)
    vec4 samples = raw.xyzw;
    #elif defined(FMT_YVYU)
    vec4 samples = raw.xwzy;
    #elif defined(FMT_UYVY)
    vec4 samples = raw.yzwx;
    #elif defined(FMT_VYUY)
    vec4 samples = raw.wzyx;
    #endif

    float y = samples.x;
    float u = samples.y-0.5;
    float v = samples.w-0.5;

    vec3 rgb;
    rgb.r = y + (1.403 * v);
    rgb.g = y - (0.344 * u) - (0.714 * v);
    rgb.b = y + (1.770 * u);

    //color *= color_matrix;
    vec3 gamma_color = pow(rgb, vec3(inv_gamma));

    gl_FragColor = vec4(gamma_color, 1);
}
